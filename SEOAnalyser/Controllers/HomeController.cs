﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SEOAnalyser.ViewModels;
using HtmlAgilityPack;

namespace SEOAnalyser.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View(new IndexViewModel());
        }

        [HttpPost]
        public IActionResult Index([FromForm] IndexViewModel vm)
        {
            var words = new Dictionary<string, int>();
            var metas = new Dictionary<string, int>();
            var links = new Dictionary<string, int>();

            var isInputUrl = false;
            var plainText = "";

            // Make sure we are on execute mode (forced hidden variable in form)
            if (!vm.Execute)
            {
                return View(vm);
            }

            // Make sure to validate before proceed.
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            // Make sure the user at least select at least one feature.
            if (!vm.EnableEachWordSearch && !vm.EnableEachWordInMetaTagsSearch && !vm.EnableExternalLinkSearch)
            {
                ModelState.AddModelError("", "Please at least select one functionality.");
                return View(vm);
            }

            // Detect the user input a url or text
            if (vm.Input.StartsWith("http"))
            {
                isInputUrl = true;
            }

            // AgilityHtmlPack to load and parse html
            var web = new HtmlWeb();
            HtmlDocument doc;

            if (isInputUrl)
            {
                try
                {
                    doc = web.Load(vm.Input);
                }
                catch
                {
                    ModelState.AddModelError("", "Unable to reach url.");
                    return View(vm);
                }
            }
            else
            {
                doc = new HtmlDocument();
                doc.LoadHtml(vm.Input);
            }

            metas = GenerateWordListFromMetaTags(doc);

            // Remove scripts to eliminate noises
            doc
                .DocumentNode
                .Descendants()
                .Where(n => n.Name == "script")
                .ToList()
                .ForEach(n => n.Remove());

            // Get all external links in the page
            if (vm.EnableExternalLinkSearch)
            {
                links = ExtractExternalLinks(vm.Input, doc, isInputUrl);
            }

            // Get the entire body innerText as a plainText
            try
            {
                plainText = doc
                    .DocumentNode
                    .SelectSingleNode("//body")
                    .InnerText;
            }
            catch
            {
                plainText = vm.Input;
            }

            // Eliminate noises.
            // Elimination based (unicode|left right space separator|multiple spaces|dash with empty spaces)
            plainText = Regex.Replace(plainText, "(&.+?;)|(\\s*[\\.\\,\\\"'\\?\\(\\)\\:!~]\\s*)|\\s+-\\s+", " ");
            plainText = Regex.Replace(plainText, "\\s+", " ");

            // Split by spaces.
            var extractedWords = plainText.Split(" ");

            // Iterate through all found words and start counting.
            foreach (var extractedWord in extractedWords)
            {
                var sanitizedWord = extractedWord
                    .ToLower()
                    .Trim();

                if (string.IsNullOrEmpty(sanitizedWord))
                {
                    continue;
                }

                if (!words.ContainsKey(sanitizedWord))
                {
                    words.Add(sanitizedWord, 1);
                }
                else
                {
                    words[sanitizedWord] += 1;
                }

                // combined feature B and feature A to reduce extra for loop
                if (vm.EnableEachWordInMetaTagsSearch && metas.ContainsKey(sanitizedWord))
                {
                    metas[sanitizedWord] += 1;
                }
            }

            // Sort based on SortBy & Desc
            IEnumerable<KeyValuePair<string, int>> sortedWords;
            IEnumerable<KeyValuePair<string, int>> sortedMetas;
            IEnumerable<KeyValuePair<string, int>> sortedLinks;

            switch (vm.SortBy)
            {
                case ESortBy.Word:
                    sortedWords = !vm.Desc ? words.OrderBy(m => m.Key) : words.OrderByDescending(m => m.Key);
                    sortedMetas = !vm.Desc ? metas.OrderBy(m => m.Key) : metas.OrderByDescending(m => m.Key);
                    sortedLinks = !vm.Desc ? links.OrderBy(m => m.Key) : links.OrderByDescending(m => m.Key);

                    break;

                case ESortBy.Occurrences:
                    sortedWords = !vm.Desc ? words.OrderBy(m => m.Value) : words.OrderByDescending(m => m.Value);
                    sortedMetas = !vm.Desc ? metas.OrderBy(m => m.Value) : metas.OrderByDescending(m => m.Value);
                    sortedLinks = !vm.Desc ? links.OrderBy(m => m.Value) : links.OrderByDescending(m => m.Value);
                    break;

                default:
                    sortedWords = words;
                    sortedMetas = metas;
                    sortedLinks = links;
                    break;
            }

            // Put the words in ViewBag so it can be used by the View
            ViewBag.Metas = sortedMetas;
            ViewBag.Words = sortedWords;
            ViewBag.Links = sortedLinks;

            return View(vm);
        }

        private Dictionary<string, int> GenerateWordListFromMetaTags(HtmlDocument doc)
        {
            var words = new Dictionary<string, int>();

            var metaNodes = doc.DocumentNode.SelectNodes("//meta");

            if (metaNodes == null)
            {
                return words;
            }

            foreach (var node in metaNodes)
            {
                var nodeName = node.GetAttributeValue("name", "").ToLower();

                if (nodeName != "keywords" && nodeName != "description")
                {
                    continue;
                }

                var nodeContent = node.GetAttributeValue("content", "");

                var extractedWords = nodeContent
                    .Replace(",", " ")
                    .Replace(".", "")
                    .Split(" ");

                foreach (var extractedWord in extractedWords)
                {
                    var sanitizedWord = extractedWord.ToLower().Trim();

                    if (string.IsNullOrEmpty(sanitizedWord))
                    {
                        continue;
                    }

                    if (!words.ContainsKey(sanitizedWord))
                    {
                        words.Add(sanitizedWord, 1);
                    }
                    else
                    {
                        words[sanitizedWord] += 1;
                    }
                }
            }

            return words;
        }

        private Dictionary<string, int> ExtractExternalLinks(string input, HtmlDocument doc, bool isInputUrl)
        {
            var links = new Dictionary<string, int>();

            // if the user input is a url
            // scan through all anchor tag and get the href attributes
            // compared it to the input url to determine external links.

            // if the user input is a text/html
            // everything still works the same except that you don't have
            // to compare with the input.

            if (doc.DocumentNode.SelectSingleNode("//body") != null)
            {
                doc
                    .DocumentNode
                    .Descendants()
                    .Where(n => n.Name == "a")
                    .ToList()
                    .ForEach(htmlNode =>
                    {
                        var nodeHref = htmlNode.GetAttributeValue("href", "");

                        if ((!nodeHref.StartsWith("http://") && !nodeHref.StartsWith("https://")) ||
                            nodeHref.StartsWith(input) ||
                            nodeHref.StartsWith("//") ||
                            nodeHref.StartsWith("/") ||
                            nodeHref.Contains("javascript:void(0)") ||
                            string.IsNullOrWhiteSpace(nodeHref))
                        {
                            return;
                        }

                        if (links.ContainsKey(nodeHref))
                        {
                            links[nodeHref] += 1;
                        }
                        else
                        {
                            links.Add(nodeHref, 1);
                        }
                    });
            }
            else
            {
                Regex
                    .Replace(input, "([\\.,]{1}\\s)|\\s{2,}", " ")
                    .Split(" ")
                    .ToList()
                    .ForEach(potentialLink =>
                    {
                        if ((!potentialLink.StartsWith("http://") && !potentialLink.StartsWith("https://")) ||
                            potentialLink.StartsWith("//") ||
                            potentialLink.StartsWith("/") ||
                            potentialLink.Contains("javascript:void(0)") ||
                            string.IsNullOrWhiteSpace(potentialLink))
                        {
                            return;
                        }

                        if (links.ContainsKey(potentialLink))
                        {
                            links[potentialLink] += 1;
                        }
                        else
                        {
                            links.Add(potentialLink, 1);
                        }
                    });
            }

            return links;
        }
    }
    
}
