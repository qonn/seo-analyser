﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SEOAnalyser.ViewModels
{
    public class IndexViewModel
    {
        public bool Execute { get; set; }

        [Required]
        public string Input { get; set; }

        public bool EnableEachWordSearch { get; set; } = true;
        public bool EnableEachWordInMetaTagsSearch { get; set; }
        public bool EnableExternalLinkSearch { get; set; }

        public ESortBy SortBy { get; set; } = ESortBy.None;
        public bool Desc { get; set; } = false;
    }

    public enum ESortBy
    {
        None,
        Word,
        Occurrences
    }
}
