# SEOAnalyser

SEOAnalyser is a simplified SEO Analyser web application that performs a simple SEO analysis of the text. User
submits a text in English or URL, page filters out stop-words (e.g. �or�, �and�, �a�, �the� etc), calculates number of
occurrences on the page of each word, number of occurrences on the page of each word listed in meta tags,
number of external links in the text.

## Installation

- https://dotnet.microsoft.com/download/dotnet-core/2.2 (SDK)

## User Guide

- Make sure to have .NET Core 2.2 SDK installed.
- Make sure to be able to call ```dotnet``` in your Command Prompt (cmd).
- Make sure to clone this repository. ```git clone https://gitlab.com/qonn/seo-analyser.git```
- Make sure to navigate to the cloned repository folder.
- Running with Visual Studio 2017:
  - Open SEOAnalyser.sln
  - On the Solution Explorer, right click Solution > Restore Nuget Package
  - Run the project
  - It should automatically navigate you to the homepage, otherwise go to https://localhost:5001

- Running without IDE:
  - Navigate to project folder
  - Double click ```dotnet-run.bat```
  - Go to https://localhost:5001

## Development

The entry point should be in ```Controllers > HomeController.cs > Index```

## Technology

- LINQ
- HTMLAgilityPack
- Bootstrap
- jQuery

## License
[MIT](https://choosealicense.com/licenses/mit/)